const ck = require("ckey");
const { Pool } = require("pg");

const PGCONFIG = Object.freeze({
  user: ck.PGUSER,
  host: ck.PGHOST,
  database: ck.PGDATABASE,
  password: ck.PGPASSWORD,
  port: ck.PGPORT,
});

const pool = new Pool(PGCONFIG);

module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback);
  },
};
