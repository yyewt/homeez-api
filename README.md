### AWS API Link
[https://apihomeez.ddns.net](https://apihomeez.ddns.net)

### Backend Hosting/Server Info
1. AWS EC2 Instance (Ubuntu)
2. PostgreSQL - in AWS EC2
3. nginx
3. Node JS
4. pm2
### How to run locally
1. Ensure PostgreSQL credential is correct in .env
2. Run the dbscripts in PostgreSQL
3. npm install
4. npm start and open [http://localhost:3001](http://localhost:3001)
