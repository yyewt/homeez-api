
CREATE TABLE Quotation (
   Q_ID serial PRIMARY KEY,
   Quotation_Info VARCHAR NOT NULL,
   Quotation_Valid BOOLEAN NOT NULL DEFAULT TRUE,
   Created_At TIMESTAMP NOT NULL DEFAULT NOW()
);

INSERT INTO QUOTATION (Quotation_Info) VALUES ('hello');
