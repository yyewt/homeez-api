const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const cors = require("cors");
const pgsql = require("./database/pgsql");
const app = express();

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const SERVER_ERROR = "Server Error";

app.get("/", function (req, res) {
  res.send("Homeez API");
});


app.get("/quotations", (req, res) => {
  const text = "SELECT * FROM Quotation ORDER BY q_id DESC";
  pgsql.query(text, (pgerr, pgres) => {
    if (pgerr) {
      return res.status(400).json({
        error: JSON.stringify(pgerr),
      });
    }

    return res.status(200).json(pgres.rows);
  });
});

app.post("/quotations", (req, res) => {
  if (!req.body.quotation_info || req.body.quotation_info.trim() === "") {
    return res.status(400).json({
      error: "Quotation Info is required",
    });
  }

  const text = "INSERT INTO Quotation(quotation_info, quotation_valid) VALUES($1, $2) RETURNING *";
  const values = [req.body.quotation_info, req.body.quotation_valid ? true: false];

  pgsql.query(text, values, (pgerr, pgres) => {
    if (pgerr) {
      return res.status(400).json({
        error: SERVER_ERROR,
      });
    }

    return res.status(200).json({
      success: true,
    });
  });
});

app.listen(3001);
